#!/bin/bash

cd scripts
# encoding with 2 different polynomials and Veritysetup
./libfec_enc.sh

# decoding erasures in code with 2 different polynomials
./libfec_erasures_dec.sh

# decoding errors in code with 2 different polynomials and Veritysetup
./libfec_errors_dec.sh

# jerasure encoding and decoding
./jerasure_erasure_enc_dec.sh

# raid algorithms encoding and decoding
./jerasure_raid_enc_dec.sh

# ldpc encoding
./ldpc_enc.sh