#!/bin/bash

# Veritysetup contains a mechanism that check corrutpion of blocks with hashes.
# However, with the interleaver that Veritysetup uses, there is no efficient way that we can find out the positions of erasures.
# We can find erasure with a precision to block. If the block was declared as erased,
# we would have to (because of the interleaver) recalculate K blocks -> not efficient.

# This means that Veritysetup cannot decode erasures.

CSV_FILE=$PWD/../results/libfec/libfec_erasures_dec.csv

LFD=$PWD/../libs/Libfec_Veritysetup/libfec-master/speed_tests
WD=$PWD

rm $CSV_FILE

for i in `seq 2 32`
do
	K=$((255-$i))

	# measure speed of decoding erasures of codes using two different polynomials
	cd $LFD
	DEC_CHAR_11d=$(./dec_erasures_char_11d 255 $K)
	DEC_CHAR_187=$(./dec_erasures_char_187 255 $K)

	# print results to stdout and to file
	LINE="$i,$DEC_CHAR_11d,$DEC_CHAR_187"
	echo $LINE >> $CSV_FILE
	echo $LINE
	unset DEC_CHAR_11d DEC_CHAR_187
done

