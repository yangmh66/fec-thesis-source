#!/bin/bash

WD=$PWD
JD=$PWD/jerasure.git
ED=$JD/Examples
LHD=$PWD/longhair-master

CSV_FILE_62=$PWD/graphs/jerasure_erasures_6_2_dec.csv
CSV_FILE_155=$PWD/graphs/jerasure_erasures_14_7_dec.csv

# first time
#cd $JD && autoreconf --force --install &&  ./configure  && make
cd $JD && make
cd $LHD && cmake . && make
rm $CSV_FILE_155 $CSV_FILE_62

#cd Examples

#echo "Enc:" && ./rs_van_enc 16 6 2
#echo "Dec:" && ./rs_van_dec 16 6 2

#echo "crs_enc:" && ./crs_enc 16 6 2
#echo "crs_dec:" && ./crs_dec 16 6 2


#echo "lh_enc:" && ./longhair_enc 6 2
#echo "lh_dec:" && ./longhair_dec 6 2
K=6
M=2

for i in `seq 1 32`
do
	cd $ED

	[ $i -eq 8 ] && RS=$(./rs_van_dec $i $K $M) && CRS=$(./crs_dec $i $K $M) && cd $LHD && LH=$(./longhair_dec $K $M) && cd $ED
	[ $i -eq 16 ] && RS=$(./rs_van_dec $i $K $M) && CRS=$(./crs_dec $i $K $M)
	[ $i -eq 32 ] && RS=$(./rs_van_dec $i $K $M) && CRS=$(./crs_dec $i $K $M)

	#sleep 5
	WORD="$i,$RS,$CRS,$LH"
	#[ -z $RS ] && WORD="$i $CRS"
	echo $WORD >> $CSV_FILE_62
	echo $WORD
	unset RS CRS LH WORD
done


K=14
M=7

for i in `seq 1 32`
do
	cd $ED
	[ $i -eq 8 ] && RS=$(./rs_van_dec $i $K $M) && CRS=$(./crs_dec $i $K $M) && cd $LHD && LH=$(./longhair_dec $K $M) && cd $ED
	[ $i -eq 16 ] && RS=$(./rs_van_dec $i $K $M) && CRS=$(./crs_dec $i $K $M)
	[ $i -eq 32 ] && RS=$(./rs_van_dec $i $K $M) && CRS=$(./crs_dec $i $K $M)
	#sleep 5
	WORD="$i,$RS,$CRS,$LH"
	#[ -z $RS ] && WORD="$i $CRS"
	echo $WORD >> $CSV_FILE_155
	echo $WORD
	unset RS CRS LH WORD
done

