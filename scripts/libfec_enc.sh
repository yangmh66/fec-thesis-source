#!/bin/bash

CSV_FILE=$PWD/../results/libfec/libfec_enc.csv

CSD=$PWD/../libs/Libfec_Veritysetup/cryptsetup-master
LFD=$PWD/../libs/Libfec_Veritysetup/libfec-master/speed_tests

IMG=tst_dev
HSH=tst_hash
FEC=tst_fec

rm $CSV_FILE
cd $CSD && dd if=/dev/urandom of=$IMG bs=1M count=500

for i in `seq 2 32`
do
	# K is number of source symbols in a codeword
	K=$((255-$i))
	cd $LFD
	# the following two examples were excluded from final results - not relevant
	#[ "$i" -eq 2 ] && ENC_8=$(./enc_8 255 $K)
	#[ "$i" -eq 2 ] && ENC_CCSDS=$(./enc_ccsds 255 $K)

	# encode to a codeword of size 255 symbols - K symbols are source symbols
	# each code uses diffrent polynomial
	ENC_CHAR_11d=$(./enc_char_11d 255 $K)
	ENC_CHAR_187=$(./enc_char_187 255 $K)

	cd $CSD

	# the time measurement is added to the Veritysetup
	#  - it can be found in file libs/Libfec_Veritysetup/cryptsetup-master/lib/verity/verity_fec.c
	#  - works both for encoding and decoding
	[ "$i" -le 22 ] && [ $(($i%2)) -eq 0 ] && ENC_VS=$(./veritysetup format tst_dev tst_hash --fec-device tst_fec --fec-roots $i --debug | grep -oP "(?<=Coding_speed: )[^ ]+")

	# print the results to stdout and to a file
	LINE="$i,$ENC_CHAR_11d,$ENC_CHAR_187,$ENC_VS"
	echo $LINE >> $CSV_FILE
	echo $LINE
	unset ENC_CHAR_11d ENC_CHAR_187 ENC_VS
done

cd $CSD && rm $IMG $HSH $FEC