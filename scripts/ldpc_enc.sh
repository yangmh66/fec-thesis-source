#!/bin/bash

# this script computes correct values
# however, these values need to be rearranged
# see the provided csv file

WD=$PWD
OWD=$PWD/../libs/openfec_v1.4.2/
RWD=$PWD/../libs/openfec_v1.4.2/bin/Release

CSV=$PWD/../results/ldpc_enc/ldpc_enc.csv

ITER=5

rm $CSV

cd $OWD #&& make clean
#cmake . && make

cd $RWD

# CR=1/2, N1=5
SUM=0
for i in `seq 1 $ITER`;
do
	RES=$(./ldpc_enc 50 100 5)
	echo "$i::$RES"
	SUM=$(($SUM+$RES))
done

AVG=$(($SUM/$ITER))
echo "CR=1/2 and N1=5,$AVG" >> $CSV
echo "CR=1/2 and N1=5,$AVG"

# CR=1/2, N1=7
SUM=0
for i in `seq 1 $ITER`;
do
	RES=$(./ldpc_enc 50 100 7)
	echo "$i::$RES"
	SUM=$(($SUM+$RES))
done

AVG=$(($SUM/$ITER))
echo "CR=1/2 and N1=7,$AVG" >> $CSV
echo "CR=1/2 and N1=7,$AVG"

# CR=1/2, N1=9
SUM=0
for i in `seq 1 $ITER`;
do
	RES=$(./ldpc_enc 50 100 9)
	echo "$i::$RES"
	SUM=$(($SUM+$RES))
done

AVG=$(($SUM/$ITER))
echo "CR=1/2 and N1=9,$AVG" >> $CSV
echo "CR=1/2 and N1=9,$AVG"

# CR=2/3, N1=5
SUM=0
for i in `seq 1 $ITER`;
do
	RES=$(./ldpc_enc 66 100 5)
	echo "$i::$RES"
	SUM=$(($SUM+$RES))
done

AVG=$(($SUM/$ITER))
echo "CR=2/3 and N1=5,$AVG" >> $CSV
echo "CR=2/3 and N1=5,$AVG"

# CR=2/3, N1=7
SUM=0
for i in `seq 1 $ITER`;
do
	RES=$(./ldpc_enc 66 100 7)
	echo "$i::$RES"
	SUM=$(($SUM+$RES))
done

AVG=$(($SUM/$ITER))
echo "CR=2/3 and N1=7,$AVG" >> $CSV
echo "CR=2/3 and N1=7,$AVG"

# CR=2/3, N1=9
SUM=0
for i in `seq 1 $ITER`;
do
	RES=$(./ldpc_enc 66 100 9)
	echo "$i::$RES"
	SUM=$(($SUM+$RES))
done

AVG=$(($SUM/$ITER))
echo "CR=2/3 and N1=9,$AVG" >> $CSV
echo "CR=2/3 and N1=9,$AVG"