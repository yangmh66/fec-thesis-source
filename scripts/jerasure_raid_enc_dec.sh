#!/bin/bash

# the tested raid algorithms works only for some values w - see manual.pdf in jerasure

SWD=$PWD/../src
JD=$PWD/../libs/jerasure.git
ED=$JD/Examples

CSV_FILE_ENC=$PWD/../results/jerasure-raid/jerasure_raid_enc.csv
CSV_FILE_DEC=$PWD/../results/jerasure-raid/jerasure_raid_dec.csv
DEV=tst

#cd $JD && make
rm $CSV_FILE_ENC $CSV_FILE_DEC

function is_prime(){
    if [[ $1 -eq 2 ]] || [[ $1 -eq 3 ]]; then
        return 1  # prime
    fi
    if [[ $(($1 % 2)) -eq 0 ]] || [[ $(($1 % 3)) -eq 0 ]]; then
        return 0  # not a prime
    fi
    j=5; w=2
    while [[ $((j * j)) -le $1 ]]; do
        if [[ $(($1 % j)) -eq 0 ]]; then
            return 0  # not a prime
        fi
        j=$((j + w))
        w=$((6 - w))
    done
    return 1  # prime
}

function remove() {

	NAMES=('tst_k1' 'tst_k2' 'tst_k3' 'tst_k4' 'tst_k5' 'tst_k6' 'tst_m1' 'tst_m2')

	cd $SWD
	RAND_I=(`./random_seq 6 2`)

	cd $ED
	rm Coding/${NAMES[${RAND_I[0]}]} Coding/${NAMES[${RAND_I[1]}]}
}

cd $ED
dd if=/dev/urandom of=$DEV bs=1M count=500

K=6
M=2
PACKETSIZE=4096

ITER=10

for i in `seq 1 32`
do
	# RS
	cd $ED
	#############
	SUM_ENC=0
	SUM_DEC=0
	[ $i -eq 8 ] &&
	for it in `seq 1 $ITER`;
	do
		RS_ENC=$(./encoder $DEV $K $M reed_sol_r6_op $i 0 0) && remove && RS_DEC=$(./decoder $DEV) && diff $DEV Coding/${DEV}_decoded
		SUM_ENC=$(($SUM_ENC+"$RS_ENC"))
		SUM_DEC=$(($SUM_DEC+$RS_DEC))
	done
	[ $SUM_ENC -ne 0 ] && RS_ENC=$(($SUM_ENC/$ITER))
	[ $SUM_DEC -ne 0 ] && RS_DEC=$(($SUM_DEC/$ITER))

	#############
	SUM_ENC=0
	SUM_DEC=0
	[ $i -eq 16 ] &&
	for it in `seq 1 $ITER`;
	do
		RS_ENC=$(./encoder $DEV $K $M reed_sol_r6_op $i 0 0) && remove && RS_DEC=$(./decoder $DEV) && diff $DEV Coding/${DEV}_decoded
		SUM_ENC=$(($SUM_ENC+$RS_ENC))
		SUM_DEC=$(($SUM_DEC+$RS_DEC))
	done
	[ $SUM_ENC -ne 0 ] && RS_ENC=$(($SUM_ENC/$ITER))
	[ $SUM_DEC -ne 0 ] && RS_DEC=$(($SUM_DEC/$ITER))

	#############
	SUM_ENC=0
	SUM_DEC=0
	[ $i -eq 32 ] &&
	for it in `seq 1 $ITER`;
	do
		RS_ENC=$(./encoder $DEV $K $M reed_sol_r6_op $i 0 0) && remove && RS_DEC=$(./decoder $DEV) && diff $DEV Coding/${DEV}_decoded
		SUM_ENC=$(($SUM_ENC+$RS_ENC))
		SUM_DEC=$(($SUM_DEC+$RS_DEC))
	done
	[ $SUM_ENC -ne 0 ] && RS_ENC=$(($SUM_ENC/$ITER))
	[ $SUM_DEC -ne 0 ] && RS_DEC=$(($SUM_DEC/$ITER))

	cd $ED
	# LIBERATION
	is_prime $i
	RET=$?
	SUM_ENC=0
	SUM_DEC=0
	[ $K -le $i ] && [ $RET -eq 1 ] &&
	for it in `seq 1 $ITER`;
	do
		L_ENC=$(./encoder $DEV $K $M liberation $i $PACKETSIZE 0) && remove && L_DEC=$(./decoder $DEV) && diff $DEV Coding/${DEV}_decoded
		SUM_ENC=$(($SUM_ENC+$L_ENC))
		SUM_DEC=$(($SUM_DEC+$L_DEC))
	done
	[ $SUM_ENC -ne 0 ] && L_ENC=$(($SUM_ENC/$ITER))
	[ $SUM_DEC -ne 0 ] && L_DEC=$(($SUM_DEC/$ITER))

	cd $ED
	# LIBER8TION
	SUM_ENC=0
	SUM_DEC=0
	[ $i -eq 8 ] &&
	for it in `seq 1 $ITER`;
	do
		L8_ENC=$(./encoder $DEV $K $M liber8tion $i $PACKETSIZE 0) && remove && L8_DEC=$(./decoder $DEV) && diff $DEV Coding/${DEV}_decoded
		SUM_ENC=$(($SUM_ENC+$L8_ENC))
		SUM_DEC=$(($SUM_DEC+$L8_DEC))
	done
	[ $SUM_ENC -ne 0 ] && L8_ENC=$(($SUM_ENC/$ITER))
	[ $SUM_DEC -ne 0 ] && L8_DEC=$(($SUM_DEC/$ITER))

	cd $ED
	# BLAUM_ROTH
	is_prime $(($i+1))
	RET=$?
	SUM_ENC=0
	SUM_DEC=0
	[ $K -le $i ] && [ $i -gt 2 ] && [ $RET -eq 1 ] &&
	for it in `seq 1 $ITER`;
	do
		BR_ENC=$(./encoder $DEV $K $M blaum_roth $i $PACKETSIZE 0) && remove && BR_DEC=$(./decoder $DEV) && diff $DEV Coding/${DEV}_decoded
		SUM_ENC=$(($SUM_ENC+$BR_ENC))
		SUM_DEC=$(($SUM_DEC+$BR_DEC))
	done
	[ $SUM_ENC -ne 0 ] && BR_ENC=$(($SUM_ENC/$ITER))
	[ $SUM_DEC -ne 0 ] && BR_DEC=$(($SUM_DEC/$ITER))


	WORD_ENC="$i,$RS_ENC,$L_ENC,$L8_ENC,$BR_ENC"
	WORD_DEC="$i,$RS_DEC,$L_DEC,$L8_DEC,$BR_DEC"

	echo "$WORD_ENC ::::::::: $WORD_DEC"

	#[ -z $RS ] && WORD="$i $CRS"
	echo $WORD_ENC >> $CSV_FILE_ENC
	echo $WORD_DEC >> $CSV_FILE_DEC
	#echo $WORD
	unset RS_ENC RS_DEC WORD RET L_ENC L_DEC L8_ENC L8_DEC BR_ENC BR_DEC SUM_ENC SUM_DEC
done

cd $ED && rm -rf Coding && rm $DEV