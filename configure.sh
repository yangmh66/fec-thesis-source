#!/bin/bash

LIBFEC_D=$PWD/libs/Libfec_Veritysetup/libfec-master
JERASURE_D=$PWD/libs/jerasure.git
OPENFEC_D=$PWD/libs/openfec_v1.4.2

# Libfec
cd $LIBFEC_D && cmake . && make && sudo make install
# now files can be compiled using -lfec (see man rs)
# example:
# in dir libs/Libfec_Veritysetup/libfec-master/speed_tests
# run: gcc -o dec_erasures_char_11d dec_erasures_char_11d.c -lfec

# Jerasure
# sudo apt-get install gf-complete-tools
cd $JERASURE_D && autoreconf --force --install && ./configure && make

# OpenFEC
cd $OPENFEC_D && make clean && cmake . && make