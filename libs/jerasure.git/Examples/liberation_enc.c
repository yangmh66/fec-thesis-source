#include <assert.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <signal.h>
#include <gf_rand.h>
#include <unistd.h>
#include <time.h>
#include <sys/time.h>
#include <sys/resource.h>
#include "jerasure.h"
#include "reed_sol.h"
#include "cauchy.h"
#include "liberation.h"

// Implemenation of liberation

// Size of device must be divisible by k*w*sizeof(long)
// by k, so we can devide device into k blocks
// by w, so device contains whole Bytes/Short words/Words
// by sizeof(long) - processor word
#define BUFF_SIZE 819200000 // one block is 4096 Bytes
#define ITERATIONS 5

#define randrange(N) rand() / (RAND_MAX/(N) + 1)

/** Encoding : takes contents of k data devices and encodes them on m coding devices
  * Decoding : takes some subset of the collection of (k+m) total devices and from the recalculates the original k devices
  * Maximum Distance Separable (MDS) code - can tolerate the loss of any m devices
  */

long time_ms(struct rusage *start, struct rusage *end)
{
	long ms;

	ms = (end->ru_utime.tv_sec - start->ru_utime.tv_sec)*1000;
	ms += (end->ru_utime.tv_usec - start->ru_utime.tv_usec)/1000;

	return ms;
}

int main (int argc, char **argv) {
	if(argc < 2) {
		printf("Word size needs to be specified.\n");
		return -1;
	}
	// Number of devices are limited, if too many entered, fails with segfault
	int k = atoi(argv[2]); // number of data devices/blocks
	int m = atoi(argv[3]); // number of coding devices/blocks
	// each coding bit is bitwise XOR os some subset of other bits
	int w = atoi(argv[1]); // word size - device is composed of w packets of equal size
	// each packet is calculated to be bitwise XOR of some subset of the other packets
	// we can perform XOR on whole computer words -> better performance
	int packetsize = 4096; // perform operation on w*packetsize blocks

	struct rusage rstart, rend; // time measurement
	long ms = 0;

	int i = 0, j = 0;

	// size of one data block
	int blocksize = BUFF_SIZE / k;
	int *bitmatrix = NULL;
	int **schedule = NULL;

	// Data buffer
	char *data = (char *)malloc(BUFF_SIZE);
	//memset(data, 1 , BUFF_SIZE);
	for(i = 0; i < BUFF_SIZE; i++) {
		data[i] = i%26+97;
	}

	// Coding data blocks
	char **dataPtr = (char **)malloc(sizeof(char *)*k);
	for(i = 0; i < k; i++) {
		dataPtr[i] = data + (i*blocksize);
	}

	// Data blocks
	char **coding = (char **)malloc(sizeof(char*)*m);
	for (i = 0; i < m; i++) {
		coding[i] = (char *)malloc(sizeof(char)*blocksize);
                if (coding[i] == NULL) { perror("malloc"); exit(1); }
	}

	// create coding matrix
	if(getrusage(RUSAGE_SELF, &rstart) < 0) {
			free(data);
			return -1;
	}

	bitmatrix = liberation_coding_bitmatrix(k, w);

	schedule = jerasure_smart_bitmatrix_to_schedule( k, m, w, bitmatrix);

	if(getrusage(RUSAGE_SELF, &rend) < 0) {
		free(data);
		return -1;
	}
		ms += time_ms(&rstart, &rend);

	// encoding
	for(i = 0; i < ITERATIONS; i++) {

		if(getrusage(RUSAGE_SELF, &rstart) < 0) {
			free(data);
			return -1;
		}
		jerasure_schedule_encode( k, m, w, schedule, dataPtr, coding, blocksize, packetsize);
		if(getrusage(RUSAGE_SELF, &rend) < 0) {
			free(data);
			return -1;
		}
		ms += time_ms(&rstart, &rend);

		for (j = 0; j < m; j++) {
        	memset(coding[j], 0, blocksize);
		}
	}
	printf("%.0f\n", ((BUFF_SIZE/1024/1024)*ITERATIONS)/(ms/1000.) );

	return 0;
}