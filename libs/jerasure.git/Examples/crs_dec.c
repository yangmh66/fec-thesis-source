#include <assert.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <signal.h>
#include <gf_rand.h>
#include <unistd.h>
#include <time.h>
#include <sys/time.h>
#include <sys/resource.h>
#include "jerasure.h"
#include "reed_sol.h"
#include "cauchy.h"
#include "liberation.h"

// Implemenation of CRS

// Size of device must be divisible by k*w*sizeof(long)
// by k, so we can devide device into k blocks
// by w, so device contains whole Bytes/Short words/Words
// by sizeof(long) - processor word
#define BUFF_SIZE 154140672 // one block is 4096 Bytes

#define ITERATIONS 100

#define randrange(N) rand() / (RAND_MAX/(N) + 1)

/** Encoding : takes contents of k data devices and encodes them on m coding devices
  * Decoding : takes some subset of the collection of (k+m) total devices and from the recalculates the original k devices
  * Maximum Distance Separable (MDS) code - can tolerate the loss of any m devices
  */

long time_ms(struct rusage *start, struct rusage *end)
{
	long ms;

	ms = (end->ru_utime.tv_sec - start->ru_utime.tv_sec)*1000;
	ms += (end->ru_utime.tv_usec - start->ru_utime.tv_usec)/1000;

	return ms;
}

int main (int argc, char **argv) {
	if(argc < 4) {
		printf("Word size needs to be specified.\n");
		return -1;
	}
	// Number of devices are limited, if too many entered, fails with segfault
	int k = atoi(argv[2]); // number of data devices/blocks
	int m = atoi(argv[3]); // number of coding devices/blocks
	// each coding bit is bitwise XOR os some subset of other bits
	int w = atoi(argv[1]); // word size - device is composed of w packets of equal size
	// each packet is calculated to be bitwise XOR of some subset of the other packets
	// we can perform XOR on whole computer words -> better performance
	int packetsize = 4096; // perform operation on w*packetsize blocks

	int i = 0, j = 0;

	struct rusage rstart, rend; // time measurement
	long ms = 0;

	// size of one data block
	int blocksize = BUFF_SIZE / k;
	int *matrix = NULL;
	int *bitmatrix = NULL;
	int **schedule = NULL;

	// Data buffer
	char *data = (char *)malloc(BUFF_SIZE);
	//memset(data, 0 , BUFF_SIZE);
	for(i = 0; i < BUFF_SIZE; i++) {
		data[i] = i%26+97;
	}
	char *cpy = (char *)malloc(BUFF_SIZE);
	memcpy(cpy, data, BUFF_SIZE);

	// Coding data blocks
	char **dataPtr = (char **)malloc(sizeof(char *)*k);
	for(i = 0; i < k; i++) {
		dataPtr[i] = data + (i*blocksize);
	}

	// Data blocks
	char **coding = (char **)malloc(sizeof(char*)*m);
	for (i = 0; i < m; i++) {
		coding[i] = (char *)malloc(sizeof(char)*blocksize);
                if (coding[i] == NULL) { perror("malloc"); exit(1); }
	}

	// create coding matrix
	matrix = cauchy_good_general_coding_matrix(k, m, w);

	bitmatrix = jerasure_matrix_to_bitmatrix(k, m, w, matrix);

	schedule = jerasure_smart_bitmatrix_to_schedule( k, m, w, bitmatrix);


	// encoding
	jerasure_schedule_encode( k, m, w, schedule, dataPtr, coding, blocksize, packetsize);

	int *erasures = (int *)malloc(sizeof(int)*(k+m));

	for(j = 0; j < ITERATIONS; j++ ) {
		// choose m random diffrent numbers, from interval [0,..,k+m]
		int max = k+m;
		int candidates[k+m];
		int vektor[m];
		srand(time(NULL));   /* Seed the random number generator. */

  		for (i=0; i<max; i++)
    		candidates[i] = i;

  		for (i = 0; i < max-1; i++) {
    		int c = randrange(max-i);
    		int t = candidates[i];
    		candidates[i] = candidates[i+c];
    		candidates[i+c] = t;
  		}

  		for (i=0; i<max; i++){
    		vektor[i] = candidates[i];
    		//printf("%d ", vektor[i]);
  		}
  		//printf("\n");

    	// declare m random blocks as erased - if can repair, it is MDS code
    	// m+1 erasures, should not be able to repair
  		for (i=0; i<m; i++) {
  			erasures[i] = vektor[i];
  			/*if( erasures[i] < k ) { // rewrite blocks by 1s to be sure that really corrected
  				memset(dataPtr[erasures[i]], 1, blocksize);
  			}else {
  				memset(coding[erasures[i] % k], 1, blocksize);
  			}*/
  		}

		erasures[m] = -1; // end of erasures in array

		if(getrusage(RUSAGE_SELF, &rstart) < 0) {
			free(data);
			return -1;
		}

		// decoding
		i = jerasure_schedule_decode_lazy(k, m, w, bitmatrix, erasures, dataPtr, coding, blocksize, packetsize, 1);

		if(getrusage(RUSAGE_SELF, &rend) < 0) {
			free(data);
			return -1;
		}
		ms += time_ms(&rstart, &rend);

		if(i == -1 || memcmp(data, cpy, BUFF_SIZE) != 0) {
			printf("[NOK]\n");
			return -1;
		}
		// encoding
		jerasure_schedule_encode( k, m, w, schedule, dataPtr, coding, blocksize, packetsize);
	}

	//printf("%ld\n", ms);
	printf("%.0f\n", ((BUFF_SIZE/1024/1024)*ITERATIONS)/(ms/1000.) );

	//free(data);

	return 0;
}