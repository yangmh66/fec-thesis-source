# CMake generated Testfile for 
# Source directory: /home/xvirgov/bth-fin/fec-thesis-source/libs/openfec_v1.4.2
# Build directory: /home/xvirgov/bth-fin/fec-thesis-source/libs/openfec_v1.4.2
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("src")
subdirs("applis/eperftool")
subdirs("applis/howto_examples/simple_client_server")
subdirs("tools/descr_stats_v1.2")
subdirs("tests")
