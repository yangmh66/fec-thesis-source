#define OF_USE_ENCODER
#define OF_USE_DECODER
#define OF_USE_REED_SOLOMON_CODEC

#define SYMBOL_LEN 4096 // in bytes
#define ITERATIONS 100000

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <string.h>
#include "../src/lib_common/of_openfec_api.h"

long time_ms(struct rusage *start, struct rusage *end)
{
	long ms;

	ms = (end->ru_utime.tv_sec - start->ru_utime.tv_sec)*1000;
	ms += (end->ru_utime.tv_usec - start->ru_utime.tv_usec)/1000;

	printf("%ld\n", ms);

	return ms;
}

int is_same(char **first, char **second, int sym_len, int numb_sym) {
	for(int i = 0; i < numb_sym; i++) {
		if(memcmp(first[i], second[i], sym_len) != 0) {
			//printf("Not same.\n");
			return 0;
		}
	}
	return 1;
}

int main(int argc, char const *argv[])
{
	printf("LDPC:\n");
	// k*SYMBOL_LENGTH bytes of data
	// (n-k)*SYMBOL_LENGTH bytes of correction data
	// (n-k) of n blocks can fail and it will be recovered -> MDS
	int k = 100;
	int n = 150;
	int esi; // encoding symbol ID
	int i, j;

	long ms = 0;
	struct rusage rstart, rend;

	of_session_t *ses;
	of_status_t stat;
	of_parameters_t *params = NULL;
	of_ldpc_parameters_t *ldpc_params = NULL;

	char **data;
	char **cpy_data; // copy of original data
	void **data_with_encoding;
	void **data_recovering;

	data = (char **)calloc(k, sizeof(char *));
	cpy_data = (char **)calloc(k, sizeof(char *));
	data_with_encoding = (void*)calloc(n, sizeof(void*));
	if (data == NULL || cpy_data == NULL || data_with_encoding == NULL) {
		printf("Error while allocating buffers.\n");
		return -1;
	}

	// Create data and point data_with_encoding to location
	for(i = 0; i < k; i++) {
		data[i] = (char *)calloc(1, SYMBOL_LEN);
		cpy_data[i] = (char *)calloc(1, SYMBOL_LEN);
		data_with_encoding[i] = (void *)data[i];
		for(j = 0; j < SYMBOL_LEN; j++) {
			data[i][j] = j%26 + 97;
			cpy_data[i][j] = j%26 + 97;
		}
	}

	if( is_same(data, cpy_data, SYMBOL_LEN, k) == 0 ) {
		printf("Arrays should't differ\n");
		return 0;
	}

	/*char *block = data_with_encoding[0];
	for(i = 0; i < SYMBOL_LEN; i++) {
		printf("%c ", block[i]);
		if(i % 32 == 0) {
			printf("\n");
		}
	}*/

	for(i = k; i < n; i++) {
		data_with_encoding[i] = (void *)calloc(1, SYMBOL_LEN);
	}

	if((stat = of_create_codec_instance(&ses, OF_CODEC_LDPC_STAIRCASE_STABLE, OF_ENCODER_AND_DECODER, 0)) != OF_STATUS_OK) {
		printf("Create codec failed.\n");
		return stat;
	}

	ldpc_params = (of_ldpc_parameters_t *)calloc(1, sizeof(* ldpc_params));
	params = (of_parameters_t *)calloc(1, sizeof(params));

	if(params == NULL || ldpc_params == NULL) {
		printf("Cannot allocate memory for params.\n");
		return -1;
	}

	ldpc_params->prng_seed = rand();
	ldpc_params->N1 = 5;

	params = (of_parameters_t *) ldpc_params;

	params->nb_source_symbols = k;
	params->nb_repair_symbols = n-k;
	params->encoding_symbol_length = SYMBOL_LEN;

	if((stat = of_set_fec_parameters(ses, params)) != OF_STATUS_OK) {
		printf("Params specification failed.\n");
		return stat;
	}


	// encode
	//printf("Encoding:");
	for(esi = params->nb_source_symbols;
		esi < params->nb_source_symbols + params->nb_repair_symbols;
		esi++) {
		// fill each block with zeroes
		memset(data_with_encoding[esi], 0, params->encoding_symbol_length);
		if((stat = of_build_repair_symbol(ses, data_with_encoding, esi)) != OF_STATUS_OK) {
			printf("Cannot build repair sybol.\n");
			return stat;
		}
	}

	printf("[OK]\n");
	if(getrusage(RUSAGE_SELF, &rstart) < 0) {
			return -1;
		}

//for(int xx = 0; xx < ITERATIONS; xx++) {
	// number blocks, from begining, that are erased
	int recoveringStart = n-k-5; // if is n-k -> it is MDS code

	// corruption
	data_recovering = (void *)calloc(n, sizeof(void *));
	for(i = 0; i < recoveringStart; i++) {
		// not discovered blocks are indicated by NULL + we write 0s at original data
		//memset(data_with_encoding[i], 0, SYMBOL_LEN);
		data_with_encoding[i] = NULL;
	}

	if( is_same(data, cpy_data, SYMBOL_LEN, k) == 1) {
		printf("Arrays should differ\n");
		return 0;
	}

	// try iterative decoding
	bool done = false;
	i = recoveringStart;
	int nChecked = 0;

	//printf("Decoding:");
	while(i < n && !done) {
		// iterare by blocks and try to decode with newly discovered block
		data_recovering[i] = data_with_encoding[i];
		if((stat = of_decode_with_new_symbol(ses, data_with_encoding[i], i)) == OF_STATUS_ERROR ) {
			printf("LDPC decoding failed.\n");
			return -1;
		}
		i++;
		nChecked++;
		if((nChecked >= k) && (of_is_decoding_complete(ses) == true)) {
			done = true;
			//printf("%i blocks discovered before recovering.\n", i-recoveringStart );
		}
	}

	if(!done && nChecked >= k) {
		stat = of_finish_decoding(ses);
		if(stat == OF_STATUS_ERROR || stat == OF_STATUS_FATAL_ERROR || stat == OF_STATUS_FAILURE ) {
			printf("LDPC decoding failed.\n");
			return -1;
		}
		else if(stat == OF_STATUS_OK) {
			done = true;
		}
	}

	if(done) {
		printf("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAa\n");
		if((stat = of_get_source_symbols_tab(ses, data_with_encoding)) != OF_STATUS_OK) {
			printf("Copying data failed.\n");
			return -1;
		}
		//printf("All source symbols rebuild after checking %u blocks.\n", nChecked);
	}
	else {
		printf("Failed to recover all erased source symbols even after checking %u blocks.\n", nChecked);
	}


//}




	/*block = (char *)data_with_encoding[0];
	for(i = 0; i < SYMBOL_LEN; i++) {
		printf("%c ", block[i]);
		if(i % 32 == 0) {
			printf("\n");
		}
	}*/



	// Release session, free memory
	if((stat = of_release_codec_instance(ses)) != OF_STATUS_OK) {
		printf("Release codec failed.\n");
		return stat;
	}

	if( is_same(data, cpy_data, SYMBOL_LEN, k) == 0) {
		printf("Arrays should't differ\n");
		return 0;
	}

	if(getrusage(RUSAGE_SELF, &rend) < 0) {
			return -1;
		}

		ms += time_ms(&rstart, &rend);
	printf("[OK]\n");

	printf("%ld\n", ms);
	printf("%lf\n", (k*SYMBOL_LEN)/1024./1024. * ITERATIONS);
printf("SPEED:%f (MiB/sec)\n", (((k*SYMBOL_LEN)/1024./1024.*ITERATIONS))/(ms/1000.) );

	// free data and correction data
	for(i = 0; i < k; i++) {
		free(data_with_encoding[i]);
	}
	free(data);
	free(data_with_encoding);
	free(data_recovering);

	return 0;
}