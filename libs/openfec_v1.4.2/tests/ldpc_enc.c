#define OF_USE_ENCODER
#define OF_USE_DECODER
#define OF_USE_REED_SOLOMON_CODEC

#define SYMBOL_LEN 1024 // in bytes
#define ITERATIONS 500000

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <sys/time.h>
#include <sys/resource.h>
#include "../src/lib_common/of_openfec_api.h"

long time_ms(struct rusage *start, struct rusage *end)
{
	long ms;

	ms = (end->ru_utime.tv_sec - start->ru_utime.tv_sec)*1000;
	ms += (end->ru_utime.tv_usec - start->ru_utime.tv_usec)/1000;

	return ms;
}


int main(int argc, char const *argv[])
{
	//printf("LDPC:\n");
	if(argc != 4) {
		printf("Wrong arguments.\n");
		return 1;
	}
	// k*SYMBOL_LENGTH bytes of data
	// (n-k)*SYMBOL_LENGTH bytes of correction data
	// (n-k) of n blocks can fail and it will be recovered -> MDS
	int k = atoi(argv[1]);
	int n = atoi(argv[2]);
	int esi; // encoding symbol ID
	int i, j;

	long ms = 0;
	struct rusage rstart, rend;

	of_session_t *ses;
	of_status_t stat;
	of_parameters_t *params = NULL;
	of_ldpc_parameters_t *ldpc_params = NULL;

	char **data;
	void **data_with_encoding;
	void **data_recovering;

	data = (char **)calloc(k, sizeof(char *));
	data_with_encoding = (void *)calloc(n, sizeof(void *));
	if(data == NULL || data_with_encoding == NULL) {
		printf("Error while allocating buffers.\n");
		return -1;
	}

	// Create data and point data_with_encoding to location
	for(i = 0; i < k; i++) {
		data[i] = (char *)calloc(1, SYMBOL_LEN);
		data_with_encoding[i] = (void *)data[i];
		for(j = 0; j < SYMBOL_LEN; j++) {
			data[i][j] = j%26 + 97;
		}
	}

	/*char *block = data_with_encoding[0];
	for(i = 0; i < SYMBOL_LEN; i++) {
		printf("%c ", block[i]);
		if(i % 32 == 0) {
			printf("\n");
		}
	}*/

	for(i = k; i < n; i++) {
		data_with_encoding[i] = (void *)calloc(1, SYMBOL_LEN);
	}

	if(getrusage(RUSAGE_SELF, &rstart) < 0) {
			return -1;
		}

	if((stat = of_create_codec_instance(&ses, OF_CODEC_LDPC_STAIRCASE_STABLE, OF_ENCODER_AND_DECODER, 0)) != OF_STATUS_OK) {
		printf("Create codec failed.\n");
		return stat;
	}

	if (getrusage(RUSAGE_SELF, &rend) < 0) {
		return -1;
	}

	ms += time_ms(&rstart, &rend);

	ldpc_params = (of_ldpc_parameters_t *)calloc(1, sizeof(* ldpc_params));
	params = (of_parameters_t *)calloc(1, sizeof(params));

	if(params == NULL || ldpc_params == NULL) {
		printf("Cannot allocate memory for params.\n");
		return -1;
	}

	ldpc_params->prng_seed = rand();
	ldpc_params->N1 = atoi(argv[3]);

	params = (of_parameters_t *) ldpc_params;

	params->nb_source_symbols = k;
	params->nb_repair_symbols = n-k;
	params->encoding_symbol_length = SYMBOL_LEN;

	if(getrusage(RUSAGE_SELF, &rstart) < 0) {
			return -1;
		}


	if((stat = of_set_fec_parameters(ses, params)) != OF_STATUS_OK) {
		printf("Params specification failed.\n");
		return stat;
	}

for(int xx = 0; xx < ITERATIONS; xx++) {
	// encode
	//printf("Encoding:");
	for(esi = params->nb_source_symbols;
		esi < params->nb_source_symbols + params->nb_repair_symbols;
		esi++) {
		// fill each block with zeroes
		//memset(data_with_encoding[esi], 0, params->encoding_symbol_length);


		if((stat = of_build_repair_symbol(ses, data_with_encoding, esi)) != OF_STATUS_OK) {
			printf("Cannot build repair sybol.\n");
			return stat;
		}

	}
}
if(getrusage(RUSAGE_SELF, &rend) < 0) {
			return -1;
		}

		ms += time_ms(&rstart, &rend);
	//printf("[OK]\n");

	//printf("%ld\n", ms);
	//printf("%lf\n", (k*SYMBOL_LEN)/1024./1024. * ITERATIONS);
	printf("%.0f", (((k*SYMBOL_LEN)/1024./1024.*ITERATIONS))/(ms/1000.) );


	// free data and correction data
	for(i = 0; i < k; i++) {
		free(data_with_encoding[i]);
	}
	free(data);
	free(data_with_encoding);
	free(data_recovering);

	return 0;
}