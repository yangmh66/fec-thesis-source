#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <sys/time.h>
#include <sys/resource.h>
#include "fec.h"

#define ITERATIONS 30
#define randrange(N) rand() / (RAND_MAX / (N) + 1)

long time_us(struct rusage *start, struct rusage *end)
{
	long us;

	us = (end->ru_utime.tv_sec - start->ru_utime.tv_sec)*1000000;
	us += (end->ru_utime.tv_usec - start->ru_utime.tv_usec);

	return us;
}

int main(int argc, const char *argv[]){
	int i;

	struct rusage rstart, rend;
	long us = 0;

	if(argc < 3) {
		printf("Arguments required\n");
		return -1;
	}
	int n = atoi(argv[1]); // n is the number of symbols in a codeword
	int m = atoi(argv[2]); // m means in this case the number of source symbols
	int pad = 255 - n;

	// calculate the number of encoding symbols
	int nroots = n-m;
	if(n+pad >= 256 || nroots != 2) {
		printf("Fail, bad attributes.\n");
		return 1;
	}
	unsigned char block[255];
	// copy of the original data
	unsigned char cpy[255-pad-nroots];

	// fill block with some data
	for(i=0;i<255-nroots;i++)
		block[i] = i%26 + 97;

	// store the original data
	memcpy(cpy, &block[pad], 255-nroots-pad);

	memset(&block[255-nroots], 0, nroots);
	// encode symbols starting at pad
	encode_rs_8(&block[pad],&block[255-nroots], pad);

	i = 0;
	// encode the block iterations times to make variation as small as possible
	for(int j = 0; j < ITERATIONS; j++) {
		// choose positions of errors and corrupt these symbols
		int max = n - pad; // low probability that only check symbols will be corrupted
		// maximum number of erasures is (n-k) rounded down
		int num = n-m; // if 1 is added, the program will fail

		int candidates[max];
		// positions of errors or erasures
		int erasures[num];

		for (i = 0; i < max; i++)
			candidates[i] = i;

		for (i = 0; i < max - 1; i++) {
			int c = randrange(max - i);
			int t = candidates[i];
			candidates[i] = candidates[i + c];
			candidates[i + c] = t;
		}

		for (i = 0; i < num; i++) {
			erasures[i] = candidates[i] + pad;
			//printf("%d ", failures[i]);
			block[candidates[i] + pad] ^= 0xff; // corrupt symbols on random positions
		}

		int fails = 0; // number of detected failures
		if(getrusage(RUSAGE_SELF, &rstart) < 0) {
			return -1;
		}
		// try to repair and find a number of failures
		fails = decode_rs_8(&block[pad], NULL, 0, pad); // NULL and 0 because we are repairing only errors

		if(getrusage(RUSAGE_SELF, &rend) < 0) {
			return -1;
		}

		printf("err: %d, detected: %d\n", num, fails);
		if(fails != num) {
			printf("Failed to decode all errors");
			return -1;
		}
		if (memcmp(&block[pad], cpy, 255 - nroots - pad) != 0) {
			printf("Failed to repair.\n");
			return -1;
		}

		//printf("[ok]\n");
		us += time_us(&rstart, &rend);
	}



	// print speed in Megabytes per second
	printf("%.0f\n",((m*ITERATIONS/1024/1024))/(us/1000000.));

	return 0;
}

