#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <sys/time.h>
#include <sys/resource.h>
#include "fec.h"

#define ITERATIONS 3000000

long time_us(struct rusage *start, struct rusage *end)
{
  long us;

  us = (end->ru_utime.tv_sec - start->ru_utime.tv_sec)*1000000;
  us += (end->ru_utime.tv_usec - start->ru_utime.tv_usec);

  return us;
}

int main(int argc, const char *argv[]){
  int i;

  struct rusage rstart, rend;
  long us = 0;

  if(argc < 3) {
    printf("Arguments required\n");
    return -1;
  }
  int n = atoi(argv[1]); // n is the number of symbols in a codeword
  int m = atoi(argv[2]); // m means in this case the number of source symbols
  int pad = 255 - n;

  // calculate the number of encoding symbols
  int nroots = n-m;
  if(n+pad >= 256 || nroots != 2) {
    printf("Fail, bad attributes.\n");
    return 1;
  }
  unsigned char block[255];

  // fill block with some data
  for(i=0;i<255-nroots;i++)
    block[i] = i%26 + 97;

  // initialize last nroots symbols of block to zero (not neccessary)
  // here will be stored encoding symbols
  // memset(&block[255-nroots], 0, nroots);

  // encode the block iterations times to make variation as small as possible
  for(i = 0; i < ITERATIONS; i++) {
    // zero the calculated encoding symbols
    memset(&block[255-nroots], 0, nroots);

    if(getrusage(RUSAGE_SELF, &rstart) < 0) {
      return -1;
    }
    // encode symbols starting at pad
    encode_rs_8(&block[pad],&block[255-nroots], pad);
    if(getrusage(RUSAGE_SELF, &rend) < 0) {
      return -1;
    }

  us += time_us(&rstart, &rend);
  }

  // print speed in Megabytes per second
  printf("%.0f\n",((m*ITERATIONS/1024/1024))/(us/1000000.));

  return 0;
}

