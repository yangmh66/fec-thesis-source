#include <iostream>
#include "ldpc4qt.h"
using namespace std;


int main()
{
    //create an ldpc code via random methods
    LDPCCode ldpccode(1000,2000,2,LDPCMakeLDPC::Evenboth,"2x2/7x3/1x7",true);

    //create an encoder for this code
    LDPCEncode encode(ldpccode);

    //make 100 blocks of random bits
    QByteArray src;
    encode.createrandomdata(100,src);

    //encode the source
    QByteArray enc;
    encode.encode(src,enc);

    return 0;
}