#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "zfec/fec.h"

int main(int argc, char const *argv[])
{
	if(argc < 3) {
		printf("Too few args\n");
		return -1;
	}
	int k = atoi(argv[1]); // how many of block are necessary to reconstruct the original data
	int m = atoi(argv[2]); // total number of blocks produced
	// 1 <= m <= 256
	// 1 <= k <= m
	// if k == m -> only splits data
	// if k == 1 -> just making m copies

	int block_size = 4096;
	int prim_bl_size = k * block_size;
	int sec_bl_size = m * block_size;

	// primary blocks = original data
	// secondary blocks = check data
	gf** primary_blocks = (gf**)malloc(k*sizeof(gf*));
	gf** secondary_blocks = (gf**)malloc((m-k)*sizeof(gf*));
	if(primary_blocks == NULL || secondary_blocks == NULL) {
		printf("Failed while allocating\n");
		return -1;
	}
	for(int i = 0; i < k; i++) {
		primary_blocks[i] = (gf*)malloc(block_size*sizeof(gf));
		for(int j = 0; j < block_size; j++) {
			primary_blocks[i][j] = i%k + 97;
		}
	}

	unsigned block_nums[m-k];
	for(int i = 0; i < (m-k); i++) {
		secondary_blocks[i] = (gf*)malloc(block_size*sizeof(gf));
		memset(secondary_blocks[i], 0, block_size);
		block_nums[i] = i+k;
	}

	gf *blck = secondary_blocks[10];
	for(int i = 0; i < block_size; i++) {
		printf("%c ", blck[i]);
		if(i % 32 == 0) {
			printf("\n");
		}
	}


	fec_t *code = fec_new(k, m);
	if(code == NULL) {
		printf("Error while creating code\n");
		return -1;
	}

	fec_encode(code, (const gf *const restrict *const restrict) primary_blocks, secondary_blocks, block_nums, m-k, block_size);

	int ii = 0;
	unsigned block_nums_packets[m];
	for(int i = m-k; i > 0; i--){
		primary_blocks[ii] = secondary_blocks[i];
		block_nums_packets[ii] = i;
		ii++;
	}

	gf** repaired = (gf**)malloc(k*sizeof(gf*));
	for(int i = 0; i < k; i++) {
		repaired[i] = (gf*)malloc(block_size*sizeof(gf));
	}
/*
    unsigned block_nums_packets[k-(m-k)];
	for(int i = 0; i < k-(m-k); i++) {
		block_nums_packets[i] = i+(m-k);
	}
	*/
	/*unsigned block_nums_packets[m-k];
	for(int i = 0; i < m-k; i++) {
		block_nums_packets[i] = i;
	}*/
	fec_decode(code,(const gf *const restrict *const restrict)  primary_blocks, repaired, block_nums_packets, block_size);

	/*if(primary_blocks == NULL) {
		printf("ERROR\n");
		return -1;
	}
	printf("\n==============================================ENCODED=========================================\n");
	//*blck = primary_blocks[1];
	for(int i = 0; i < block_size; i++) {
		printf("%d ", blck[i]);
		if(i % 32 == 0) {
			printf("\n");
		}
	}*/
	printf("[OK]\n");
	return 0;
}