# CMake generated Testfile for 
# Source directory: /home/xvirgov/BT/libs/libcorrect-master/tests
# Build directory: /home/xvirgov/BT/libs/libcorrect-master/tests
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(convolutional_test "/home/xvirgov/BT/libs/libcorrect-master/tests/convolutional_test_runner")
set_tests_properties(convolutional_test PROPERTIES  WORKING_DIRECTORY "/home/xvirgov/BT/libs/libcorrect-master/tests")
add_test(convolutional_sse_test "/home/xvirgov/BT/libs/libcorrect-master/tests/convolutional_sse_test_runner")
set_tests_properties(convolutional_sse_test PROPERTIES  WORKING_DIRECTORY "/home/xvirgov/BT/libs/libcorrect-master/tests")
add_test(convolutional_shim_test "/home/xvirgov/BT/libs/libcorrect-master/tests/convolutional_shim_test_runner")
set_tests_properties(convolutional_shim_test PROPERTIES  WORKING_DIRECTORY "/home/xvirgov/BT/libs/libcorrect-master/tests")
add_test(reed_solomon_test "/home/xvirgov/BT/libs/libcorrect-master/tests/reed_solomon_test_runner")
set_tests_properties(reed_solomon_test PROPERTIES  WORKING_DIRECTORY "/home/xvirgov/BT/libs/libcorrect-master/tests")
add_test(reed_solomon_shim_interop_test "/home/xvirgov/BT/libs/libcorrect-master/tests/reed_solomon_shim_interop_test_runner")
set_tests_properties(reed_solomon_shim_interop_test PROPERTIES  WORKING_DIRECTORY "/home/xvirgov/BT/libs/libcorrect-master/tests")
