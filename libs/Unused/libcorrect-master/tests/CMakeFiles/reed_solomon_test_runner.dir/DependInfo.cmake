# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/home/xvirgov/BT/libs/libcorrect-master/tests/reed-solomon.c" "/home/xvirgov/BT/libs/libcorrect-master/tests/CMakeFiles/reed_solomon_test_runner.dir/reed-solomon.c.o"
  "/home/xvirgov/BT/libs/libcorrect-master/tests/rs_tester.c" "/home/xvirgov/BT/libs/libcorrect-master/tests/CMakeFiles/reed_solomon_test_runner.dir/rs_tester.c.o"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "HAVE_SSE=1"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "include"
  "tests/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/xvirgov/BT/libs/libcorrect-master/CMakeFiles/correct_static.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
