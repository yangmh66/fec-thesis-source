#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <string.h>

// measures the speed of memcpy with functions getrusage and clock_gettime

#define BUFF_SIZE 10485760
#define ITERATIONS 2000

long time_ms(struct rusage *start, struct rusage *end)
{
	long ms;

	ms = (end->ru_utime.tv_sec - start->ru_utime.tv_sec)*1000;
	ms += (end->ru_utime.tv_usec - start->ru_utime.tv_usec)/1000;

	return ms;
}

long timespec_ms(struct timespec *start, struct timespec *end) {
	return (end->tv_sec - start->tv_sec) * 1000 +
			(end->tv_nsec - start->tv_nsec) / (1000 * 1000);
}

int main() {
	struct rusage rstart, rend;
	struct timespec tstart, tend;
	long ms = 0;

	char *src = (char *)malloc(BUFF_SIZE);
	char *dest = (char *)malloc(BUFF_SIZE);
memset(src, 'a', BUFF_SIZE);
	for(int i = 0; i < ITERATIONS; i++) {
		//memset(src, (char) (i % 128), BUFF_SIZE);

		/*if(getrusage(RUSAGE_SELF, &rstart) < 0) {
			free(src);
			free(dest);
			return -1;
		}*/

		if(clock_gettime(CLOCK_MONOTONIC_RAW, &tstart) < 0) {
			free(src);
			free(dest);
			return -1;
		}

		memcpy(dest, src, BUFF_SIZE);

		if(clock_gettime(CLOCK_MONOTONIC_RAW, &tend) < 0) {
			free(src);
			free(dest);
			return -1;
		}

		/*if(getrusage(RUSAGE_SELF, &rend) < 0) {
			free(src);
			free(dest);
			return -1;
		}*/
		//ms += time_ms(&rstart, &rend);

		ms += timespec_ms(&tstart, &tend);
	}

	printf("SPEED:%f (MiB/sec)\n", ((BUFF_SIZE/1024/1024)*ITERATIONS)/(ms/1000.) );

	free(src);
	free(dest);
	return 0;
}
