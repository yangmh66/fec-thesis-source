#include <stdio.h>
#include <stdlib.h>
#include <time.h>

// prints random unique elements
// max value is first paramter
// number of values to print is second

#define randrange(N) rand() / (RAND_MAX / (N) + 1)

/**
  * Prints repeating integers in array.
  */
void printRepeating(int arr[], int size)
{
	int i, j;
	for (i = 0; i < size; i++)
		for (j = i + 1; j < size; j++)
			if (arr[i] == arr[j]) {
				printf("Found repeating element: %d ", arr[i]);
				return;
			}

	printf("No repeating.\n");
}

int main(int argc, char** argv)
{
	if (argc < 3)
		return -1;

	int max = atoi(argv[1]);
	int num = atoi(argv[2]);

	int candidates[max];
	int failures[num]; // positions of errors or erasures
	srand(time(NULL));

	int i = 0;
	for (i = 0; i < max; i++)
		candidates[i] = i;

	for (i = 0; i < max - 1; i++) {
		int c = randrange(max - i);
		int t = candidates[i];
		candidates[i] = candidates[i + c];
		candidates[i + c] = t;
	}

	for (i = 0; i < num; i++) {
		failures[i] = candidates[i];
		printf("%d ", failures[i]);
	}

	//printRepeating(failures, num);

	return 0;
}